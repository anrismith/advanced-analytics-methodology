# Advanced Analytics Methodology

## Project Objective:
This project aims to investigate new measurment techniques for the partner Advanced Analytics reports. The frequency of reporting has recently been extended from triannual to bianually. The concern is that the current method of uplift measurement is no longer accurate as too musch time has lapsed to continue comapring member spend to base spend. (particularly for the members that joined the program in 2013)  


## Project Scope:
- Report Output needs to be in line with the current output
- Hold all items the same, except for the way in which spend uplift is calculated. Member base spend will essentially be swapped out for Matched Non-member spend at the beginning of the period.


## Current Methodology
Currently uplift is measured through measuring what members used to spend at a RR before they joined the progam and how much they are spending there now. 
As time has gone by however this comparison is becoming less accuarate as the underlying characteristics determining spend behaviour have changed. 
(Gotten older, earn more, family dynamic has changed)


## Testing methodology:

1. Match members and non-members at the start of the period and measure the difference in their spend as uplift. ie instead of using members spend previously to measure uplift we will rather compare member spend to that of similar non-members and take the difference to be uplift.
2. Match members based on the following characteristics:
	-age
	-gender
	-derived income
	-spend at given RR, Competitor